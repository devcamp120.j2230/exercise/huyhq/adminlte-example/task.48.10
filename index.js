/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gREQUEST_STATUS_OK = 200;
const gREQUEST_CREATE_OK = 201; // status 201 là tạo mới thành công
const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;

var gBASE_URL = "https://62454a477701ec8f724fb923.mockapi.io/api/v1/";
// Biến mảng toàn cục chứa danh sách tên các thuộc tính
const gUSER_COLS = ["stt", "studentId", "subjectId", "grade", "examDate", "action"];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gUSER_ID_COL = 0;
const gUSER_FULLNAME_COL = 1;
const gUSER_SUBJECT_COL = 2;
const gUSER_SCORE_COL = 3;
const gUSER_EXAMDATE_COL = 4;
const gUSER_ACTION_COL = 5;

// đối tượng grade sẽ được tạo mới
var gGradeObjRequest = {
    studentId: "",
    subjectId: "",
    grade: "",
    examDate: ""
};

var gID = null;        //id đối tượng
var gJsonStudent = ""; //đối tượng chứa danh sách sinh viên

var gJsonSubject = ""; //đối tượng chứa danh sách môn học

var gJsonGrade = ""; //đối tượng chứa danh sách điểm


/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    // API lấy danh sách sinh viên (Students)
    onGetStudentListClick()
    // API lấy danh sách môn học (Subjects)
    onGetSubjectListClick();
    // API lấy danh sách điểm - get all (grades)
    onBtnGetAllGradeClick();
    //them diem click button
    onClickBtnThemDiem();
    //click button confirm them diem
    onClickBtnThemDiemConfirm();
    //load danh sách sinh viên filter
    getDataStudentSelect(gJsonStudent, "select-student-filter");
    //load danh sách môn học filter
    getDataSubjectSelect(gJsonSubject, "select-subject-filter");
    //click filter button
    onClickBtnFilter();
})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//ham them diem
function onClickBtnThemDiem() {
    $("#btn-create-new").on("click", function () {
        //show modal
        $("#modal-create").modal("show");
        //them du lieu vao select student
        getDataStudentSelect(gJsonStudent, "select-student-create");
        //them di lieu vao select subject
        getDataSubjectSelect(gJsonSubject, "select-subject-create");
    })
}
//ham confirm them diem
function onClickBtnThemDiemConfirm() {
    $("#btn-confirm-create").on("click", function () {
        //them du lieu form vao Obj
        themDuLieuObj(gGradeObjRequest, "create");
        //kiem tra du lieu
        if (validateObj(gGradeObjRequest)) {
            //hide modal
            $("#modal-create").modal("hide");
            alert("Nhập điểm thành công!");
            onCreateGradeClick(gGradeObjRequest)
        }
    })
}
// hàm xử lý khi click vào nút user detail
function onUserEditClick(paramUserDetailBtn) {
    var vUserDetailBtn = $(paramUserDetailBtn);
    var gUserTable = $("#table-users").DataTable();
    var vTableRow = vUserDetailBtn.parents("tr");
    var vDataTableRow = gUserTable.row(vTableRow);
    var vData = vDataTableRow.data();
    gID = vData.id;
    //them du lieu vao select student
    getDataStudentSelect(gJsonStudent, "select-student-edit");
    //them di lieu vao select subject
    getDataSubjectSelect(gJsonSubject, "select-subject-edit");
    //them du lieu vao form
    $("#select-student-edit").val(vData.studentId);
    $("#select-subject-edit").val(vData.subjectId);
    $("#inp-grade-edit").val(vData.grade);
    $("#inp-time-edit").val(vData.examDate);
    //show modal
    $("#modal-edit").modal("show");
}
//hàm xử lý khi click button xóa
function onClickDelete(paramStudent) {
    var vUserDetailBtn = $(paramStudent);
    var gUserTable = $("#table-users").DataTable();
    var vTableRow = vUserDetailBtn.parents("tr");
    var vDataTableRow = gUserTable.row(vTableRow);
    var vData = vDataTableRow.data();
    gID = vData.id;
    $("#delete-confirm-modal").modal("show");
}
//ham xu ly khi confirm delete
function onClickConfirmDelete() {
    //goi API
    onDeleteGradeClick(gID);
    //xu ly fronend
    $("#delete-confirm-modal").modal("hide");

}
//hàm xử lý khi click filter
function onClickBtnFilter(vparamObj) {
    $("#btn-filter").on("click", function () {
        onBtnGetAllGradeClick();
    })
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm thêm data vào datatable
function getDataOnDatatable(gJsonObj) {
    // var gUserObj = JSON.parse(gJsonUser);
    $("#table-users").DataTable().destroy();
    $("#table-users").DataTable({
        data: gJsonObj,
        columns: [
            { data: gUSER_COLS[gUSER_ID_COL] },
            { data: gUSER_COLS[gUSER_FULLNAME_COL] },
            { data: gUSER_COLS[gUSER_SUBJECT_COL] },
            { data: gUSER_COLS[gUSER_SCORE_COL] },
            { data: gUSER_COLS[gUSER_EXAMDATE_COL] },
            { data: gUSER_COLS[gUSER_ACTION_COL] }
        ],
        columnDefs: [
            {
                targets: gUSER_ID_COL,
                render: function (data, type, row, meta) {
                    return (meta.row + 1);
                }
            },
            {
                targets: gUSER_FULLNAME_COL,
                render: getFullName
            },
            {
                targets: gUSER_SUBJECT_COL,
                render: getSubjectName
            },
            {
                targets: gUSER_ACTION_COL,
                defaultContent: `
                <img class="edit-grade" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                <img class="delete-grade" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
              `
            }
        ]
    });
    //click edit
    $("#table-users tbody").on("click", ".edit-grade", function () {
        onUserEditClick(this);
    });
    //click confirm edit
    $("#btn-confirm-edit").on("click", function () {
        //them du lieu form vao Obj
        themDuLieuObj(gGradeObjRequest, "edit");
        if (validateObj(gGradeObjRequest)) {
            //hide modal
            $("#modal-edit").modal("hide");
            alert("Sửa điểm thành công!");
            onUpdateGradeClick(gID, gGradeObjRequest);
        }

    })
    //click delete
    $("#table-users tbody").on("click", ".delete-grade", function () {
        onClickDelete(this);
    });
    //click confirm delete
    $("#btn-confirm-delete").on("click", function () {
        console.log(gID);
        onClickConfirmDelete(gID);
    })
}

// API lấy danh sách sinh viên (Students)
function onGetStudentListClick() {
    var vAPI_URL = gBASE_URL + "/students";
    var vXhttp = new XMLHttpRequest();
    vXhttp.onreadystatechange = function () {
        if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
            // console.log(vXhttp.responseText); // ghi response text ra console.log
            gJsonStudent = JSON.parse(vXhttp.responseText);
        }
    };
    vXhttp.open("GET", vAPI_URL, false);
    vXhttp.send();
}
// API lấy danh sách môn học (Subjects)
function onGetSubjectListClick() {
    var vAPI_URL = gBASE_URL + "/subjects";
    var vXhttp = new XMLHttpRequest();
    vXhttp.onreadystatechange = function () {
        if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
            // console.log(vXhttp.responseText); //ghi response text ra console.log
            gJsonSubject = JSON.parse(vXhttp.responseText);
        }
    };
    vXhttp.open("GET", vAPI_URL, false);
    vXhttp.send();
}

// API lấy danh sách điểm - get all (grades)
function onBtnGetAllGradeClick() {
    var vAPI_URL = gBASE_URL + "/grades";
    // create a request
    var vXmlHttp = new XMLHttpRequest();
    vXmlHttp.open("GET", vAPI_URL, true);
    vXmlHttp.send();
    vXmlHttp.onreadystatechange = function () {
        if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
            // console.log(vXmlHttp.responseText);
            var gJsonGrade = JSON.parse(vXmlHttp.responseText);
            var vFilterObj = filterStudent(gJsonGrade);
            console.log(vFilterObj);
            //thêm data vào datatable
            getDataOnDatatable(vFilterObj);
        }
    }
}
// API tạo (Create) một grade mới
function onCreateGradeClick(vGradeObjRequest) {
    const vAPI_URL = gBASE_URL + "/grades";
    var vXmlHttp = new XMLHttpRequest();
    vXmlHttp.open("POST", vAPI_URL, true);
    vXmlHttp.setRequestHeader("Content-Type", "application/json");
    vXmlHttp.send(JSON.stringify(vGradeObjRequest));

    vXmlHttp.onreadystatechange = function () {
        if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_CREATE_OK) {

            //lấy danh sách điểm mới
            onBtnGetAllGradeClick();
        }
    }
}
// API để sửa (update) một grade theo Id
function onUpdateGradeClick(vGradeId, vGradeObjRequest) {
    const vAPI_URL = gBASE_URL + "/grades/";
    var vXmlHttp = new XMLHttpRequest();
    vXmlHttp.open("PUT", vAPI_URL + vGradeId, true);
    vXmlHttp.setRequestHeader("Content-Type", "application/json");
    vXmlHttp.send(JSON.stringify(vGradeObjRequest));

    vXmlHttp.onreadystatechange = function () {
        if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
            //lấy danh sách điểm mới
            onBtnGetAllGradeClick();
        }
    }
}

// API để xóa (delete) một grade theo Id
function onDeleteGradeClick(vGradeId) {
    const vAPI_URL = gBASE_URL + "/grades/";
    // id grade sẽ xóa

    var vXmlhttp = new XMLHttpRequest();
    vXmlhttp.open("DELETE", vAPI_URL + vGradeId, true);
    vXmlhttp.send();

    vXmlhttp.onreadystatechange = function () {
        // xóa thành công user
        if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
            alert("Delete user " + vGradeId + " thành công!");
            //goi Api Grade
            onBtnGetAllGradeClick();
        }
    }
}
//lấy thông tin fullname user
function getFullName(data, type, row, meta) {
    for (let i = 0; i < gJsonStudent.length; i++) {
        if (data == gJsonStudent[i].id) {
            return gJsonStudent[i].firstname + " " + gJsonStudent[i].lastname;
        }

    }
}
//lấy thông tin môn học
function getSubjectName(data, type, row, meta) {
    for (let i = 0; i < gJsonSubject.length; i++) {
        if (data == gJsonSubject[i].id) {
            return gJsonSubject[i].subjectName;
        }
    }
}
//ham them du lieu sinh vien vao select
function getDataStudentSelect(paramObj, id) {
    var vSelectElm = $("#" + id);
    for (let i = 0; i < paramObj.length; i++) {
        $("<option>", {
            value: paramObj[i].id,
            html: paramObj[i].firstname + " " + paramObj[i].lastname
        }).appendTo(vSelectElm);
    }
}
//ham them du lieu mon hoc vao select create
function getDataSubjectSelect(paramObj, id) {
    var vSelectElm = $("#" + id);
    for (let i = 0; i < paramObj.length; i++) {
        $("<option>", {
            value: paramObj[i].id,
            html: paramObj[i].subjectName
        }).appendTo(vSelectElm);
    }
}
//them du lieu ObjRequest
function themDuLieuObj(paramObj, id) {
    paramObj.studentId = $("#select-student-" + id).val();
    paramObj.subjectId = $("#select-subject-" + id).val();
    paramObj.grade = $("#inp-grade-" + id).val();
    paramObj.examDate = $("#inp-time-" + id).val();
}
//kiem tra du lieu ObjRequest
function validateObj(paramObj) {
    if (paramObj.studentId == 0) {
        alert("Hãy chọn tên sinh viên");
        return false;
    }

    if (paramObj.subjectId == 0) {
        alert("Hãy chọn tên môn học");
        return false;
    }
    if (paramObj.grade == "") {
        alert("Hãy nhập diem");
        return false;
    }
    var grade = parseInt(paramObj.grade);
    if (grade < 0 || grade > 10) {
        alert("Điểm không hợp lệ");
        return false;
    }
    if (paramObj.examDate == "") {
        alert("Hãy nhập ngày thi");
        return false;
    }
    return true;
}
//filter theo ten mon hoc
function filterStudent(vparamObj){
    var vStudentFilter = $("#select-student-filter").val();
    var vSubjectFilter = $("#select-subject-filter").val();
    if(vStudentFilter != "0" || vSubjectFilter != "0"){
        var vNewObj = vparamObj.filter(function (vObj) {
            if(vStudentFilter == "0"){
                return vObj.subjectId == vSubjectFilter;
            }else if(vSubjectFilter == "0"){
                return vObj.studentId == vStudentFilter;
            }else{
                return (vObj.studentId == vStudentFilter && vObj.subjectId == vSubjectFilter);
            }
            
        })
        return vNewObj;
    }
    return vparamObj;
}